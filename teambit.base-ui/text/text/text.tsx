import React, { ReactNode, HTMLAttributes } from 'react';

export type TextProps = {
  /**
   * children.
   */
  children: ReactNode;
} & React.HTMLAttributes<HTMLSpanElement>;

/**
 * Generic text component.
 * TODO: @amir and @oded to define this component API.
 */
export function Text({ children, ...rest }: TextProps) {
  return <span {...rest}>{children}</span>;
}
